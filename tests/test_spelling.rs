use std::sync::OnceLock;

use toy_spelling_corrector::*;

static SPELL_CHECKER: OnceLock<SpellChecker> = OnceLock::new();

#[test]
fn test_spelling() {
    let sc = SPELL_CHECKER.get_or_init(|| {
        let buf = std::fs::read_to_string("big.txt").unwrap();
        SpellChecker::new(&buf)
    });
    // insert
    assert_eq!(&sc.correction("speling"), "spelling");
    // replace 2
    assert_eq!(&sc.correction("korrectud"), "corrected");
    // replace
    assert_eq!(&sc.correction("bycycle"), "bicycle");
    // insert 2
    assert_eq!(&sc.correction("inconvient"), "inconvenient");
    // delete
    assert_eq!(&sc.correction("arrainged"), "arranged");
    // transpose
    assert_eq!(&sc.correction("peotry"), "poetry");
    // transpose + delete
    assert_eq!(&sc.correction("peotryy"), "poetry");
    // known
    assert_eq!(&sc.correction("word"), "word");
    // unknown
    assert_eq!(&sc.correction("quintessential"), "quintessential");
}
