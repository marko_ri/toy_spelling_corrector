use regex::Regex;
use std::collections::HashMap;

fn all_words(text: &str) -> Vec<String> {
    let re = Regex::new(r"\w+").unwrap();
    re.captures_iter(text).map(|c| c[0].to_string()).collect()
}

fn count_words(words: Vec<String>) -> HashMap<String, u32> {
    let mut result = HashMap::new();
    for word in words.into_iter() {
        result
            .entry(word)
            .and_modify(|counter| *counter += 1)
            .or_insert(1);
    }
    result
}

pub struct SpellChecker {
    word_freq: HashMap<String, u32>,
}

impl SpellChecker {
    pub fn new(corpus: &str) -> Self {
        Self {
            word_freq: count_words(all_words(corpus)),
        }
    }

    // The subset of words that appear in the dictionary of word_freq.
    fn known(&self, words: Vec<String>) -> Vec<String> {
        words
            .into_iter()
            .filter(|w| self.word_freq.contains_key(w))
            .collect()
    }

    // Most probable spelling correction for word
    pub fn correction(&self, word: &str) -> String {
        self.candidates(word)
            .into_iter()
            .max_by_key(|w| self.word_freq.get(w).unwrap_or(&0))
            .unwrap()
    }

    // Generate possible spelling corrections for word
    fn candidates(&self, word: &str) -> Vec<String> {
        let no_edits = self.known(vec![word.to_string()]);
        if !no_edits.is_empty() {
            return no_edits;
        }

        let edits1 = self.known(edits1(word));
        if !edits1.is_empty() {
            return edits1;
        }

        let edits2 = self.known(edits2(word, &self.word_freq));
        if !edits2.is_empty() {
            return edits2;
        }

        vec![word.to_string()]
    }
}

// All edits that are two edits away from word
fn edits2(word: &str, word_freq: &HashMap<String, u32>) -> Vec<String> {
    let mut result = Vec::new();
    for e1 in edits1(word).iter() {
        for e2 in edits1(e1).into_iter() {
            if word_freq.contains_key(&e2) {
                result.push(e2);
            }
        }
    }
    result.sort();
    result.dedup();

    result
}

// All edits that are one edit away from word
fn edits1(word: &str) -> Vec<String> {
    let mut result = Vec::new();
    let letters = "abcdefghijklmnopqrstuvwxyz";
    let splits = splits(word);
    result.append(&mut deletes(&splits));
    result.append(&mut transposes(&splits));
    result.append(&mut replaces(&splits, letters));
    result.append(&mut inserts(&splits, letters));
    result.sort();
    result.dedup();

    result
}

fn splits(word: &str) -> Vec<(&str, &str)> {
    (0..word.len() + 1)
        .into_iter()
        .map(|i| word.split_at(i))
        .collect()
}

// remove one letter
fn deletes(splits: &[(&str, &str)]) -> Vec<String> {
    splits
        .into_iter()
        .filter(|&(_l, r)| r.len() > 0)
        .map(|(l, r)| l.to_string() + &r[1..])
        .collect()
}

// swap two adjacent letters
fn transposes(splits: &[(&str, &str)]) -> Vec<String> {
    splits
        .into_iter()
        .filter(|&(_l, r)| r.len() > 1)
        .map(|(l, r)| l.to_string() + &r[1..2] + &r[0..1] + &r[2..])
        .collect()
}

// change one letter to another
fn replaces(splits: &[(&str, &str)], letters: &str) -> Vec<String> {
    splits
        .into_iter()
        .filter(|&(_l, r)| r.len() > 0)
        .fold(Vec::new(), |mut acc, (l, r)| {
            for letter in letters.chars() {
                acc.push(l.to_string() + &letter.to_string() + &r[1..])
            }
            acc
        })
}

// add a letter
fn inserts(splits: &[(&str, &str)], letters: &str) -> Vec<String> {
    splits.into_iter().fold(Vec::new(), |mut acc, (l, r)| {
        for letter in letters.chars() {
            acc.push(l.to_string() + &letter.to_string() + &r[0..])
        }
        acc
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all_words() {
        let input = "This is a test, isn't it? TEST".to_lowercase();
        let expected = vec!["this", "is", "a", "test", "isn", "t", "it", "test"];
        assert_eq!(expected, all_words(&input));
    }

    #[test]
    fn test_count_words() {
        let input = vec![
            "foo".to_string(),
            "bar".to_string(),
            "foo".to_string(),
            "foo".to_string(),
            "bar".to_string(),
        ];
        let counter = count_words(input);
        assert_eq!(2, counter.len());
        assert_eq!(Some(&3), counter.get("foo"));
        assert_eq!(Some(&2), counter.get("bar"));
        assert_eq!(None, counter.get("baz"));
    }

    #[test]
    fn test_edits1() {
        let input = "something";
        let expected = edits1(input);
        assert!(expected.contains(&"somethin".to_string()));
        assert!(expected.contains(&"omething".to_string()));
        assert!(expected.contains(&"somehting".to_string()));
        assert!(expected.contains(&"bsomething".to_string()));
        assert!(expected.contains(&"somethingb".to_string()));
        assert!(expected.contains(&"sometring".to_string()));
        assert_eq!(expected.len(), 494);
    }

    #[test]
    fn test_splits() {
        let word = "hello";
        let expected = vec![
            ("", "hello"),
            ("h", "ello"),
            ("he", "llo"),
            ("hel", "lo"),
            ("hell", "o"),
            ("hello", ""),
        ];
        assert_eq!(expected, splits(word));
    }

    #[test]
    fn test_deletes() {
        let word = "hello";
        let expected = vec!["ello", "hllo", "helo", "helo", "hell"];
        assert_eq!(expected, deletes(&splits(word)));
    }

    #[test]
    fn test_transposes() {
        let word = "hello";
        let expected = vec!["ehllo", "hlelo", "hello", "helol"];
        assert_eq!(expected, transposes(&splits(word)));
    }

    #[test]
    fn test_replaces() {
        let word = "hello";
        let alphabet = "ab";
        let expected = vec![
            "aello", "bello", "hallo", "hbllo", "healo", "heblo", "helao", "helbo", "hella",
            "hellb",
        ];
        assert_eq!(expected, replaces(&splits(word), alphabet));
    }

    #[test]
    fn test_inserts() {
        let word = "hello";
        let alphabet = "ab";
        let expected = vec![
            "ahello", "bhello", "haello", "hbello", "heallo", "hebllo", "helalo", "helblo",
            "hellao", "hellbo", "helloa", "hellob",
        ];
        assert_eq!(expected, inserts(&splits(word), alphabet));
    }
}
