
Toy spelling corrector in Rust, based on the 2007 article: [How to Write a Spelling Corrector](https://norvig.com/spell-correct.html).
